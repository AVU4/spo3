#include "../header/global_structures.h"
#include "../header/ui_module.h"
#include "../header/utils.h"


void free_context(struct global_structure *global_structure)
{
    destroy_ui(global_structure->windows_state);

    GList *current_element = g_list_first(global_structure->files_list);
    while(current_element != NULL)
    {
        free_file(current_element->data);
        current_element = current_element->next;
    }
    g_list_free(global_structure->files_list);

    free(global_structure);
}
