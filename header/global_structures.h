#include <glib.h>
#include <curses.h>
#include <netinet/in.h>

#pragma once

struct files_list_wrapper
{
    GList *files_list;
};


struct file
{
    char *name;
    int size;
    char *md5_hash;
    char *relative_path;
};

struct downloading_file
{
    char *file_description;
    char *message;
};


struct uploading_file
{
    char *file_description;
    char *message;
};

struct node_description
{
    int port;
    int address;
};

struct tcp_port_description
{
    int port;
    int socket_fd;
    struct sockaddr_in address;
};

struct tcp_listener_thread_struct
{
    struct global_structure *global_structure;
    struct tcp_port_description *tcp_port_description;
    struct file *file;
    char *file_description;
};

struct tcp_client_thread_struct
{
    struct global_structure *global_structure;
    struct node_description *node_description;
    struct file *file;
    char *file_description;
    int file_offset;
    int file_length;
    int total_file_length;
};



struct windows
{
    WINDOW *header_window1;
    WINDOW *header_window2;
    WINDOW *downloading_window;
    WINDOW *uploading_window;
    WINDOW *log_window;
    WINDOW *command_line_window;
};


struct windows_state
{
    struct windows *windows;
    GList *logs;
    GList *uploading_files;
    GList *downloading_files;
    pthread_mutex_t logs_mutex;
    pthread_mutex_t uploading_mutex;
    pthread_mutex_t downloading_mutex;
};


struct global_structure {
    char *root_path;
    struct windows_state *windows_state;
    GList *files_list;
    char close_program;
};


void free_context(struct global_structure *global_structure);
