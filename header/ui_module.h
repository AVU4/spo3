#include "./global_structures.h"





void draw_header_windows(WINDOW *header_window1, WINDOW *header_window2);

void draw_downloading_window(struct windows_state *windows_state);

void draw_uploading_window(struct windows_state *windows_state);

void draw_log_window(struct windows_state *windows_state);

void draw_command_line_window(WINDOW *command_line_window);

void ui_print(struct windows_state *windows_state, char *string, char show_time);

void draw_ui(struct global_structure *global_structure);

void destroy_ui(struct windows_state *windows_state);

void update_downloading_file(struct windows_state *windows_state, struct downloading_file *downloading_file);

void update_uploading_file(struct windows_state *windows_state, struct uploading_file *uploading_file);
